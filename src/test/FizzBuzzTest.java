import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class FizzBuzzTest {
    private FizzBuzz fb = new FizzBuzz();
    @Test
    void returnFizzFor3() {
        assertEquals("Fizz", fb.processMethod(3));
    }

    @Test
    void returnFizzFor5() {
        assertEquals("Buzz", fb.processMethod(5));
    }

    @Test
    void returnFizzBuzzFor15() {
        assertEquals("FizzBuzz", fb.processMethod(15));
    }

    @Test
    void returnZeroFor0() {
        assertEquals("0", fb.processMethod(0));
    }

    @Test
    void returnNumberFor11() {
        assertEquals("11", fb.processMethod(11));
    }
}
