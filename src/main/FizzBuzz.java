class FizzBuzz {

    private static final int Three = 3;
    private static final int Five = 5;

    String processMethod(int number) {

        StringBuilder result = new StringBuilder();

        if (number == 0) {
            return Integer.toString(number);
        }
        if (number % Three == 0) {
            result.append("Fizz").toString();
        }
        if (number % Five == 0) {
            result.append("Buzz").toString();
        }
        if (result.toString().isEmpty())
            return Integer.toString(number);
        else
            return result.toString();
    }
}
